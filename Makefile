# Minimal Makefile for v5 go prototye
VERSION    := $(shell cat ./VERSION)
IMAGE_NAME := radio/go-apikey-service

# No args operation builds the binary
all: build

# Run linting
check:
	golint $(go list ./...) && megacheck ./...

# Build a binary named `app` in `build` directory
build:
	mkdir -p build
	dep ensure
	go build -o build/app

# Pull dependencies
ensure:
	dep ensure

# Run the code without building a binary
run:
	go run main.go

# Run all tests
test:
	go test ./...

# Create a docker image locally (builds its own binary in `build/docker`)
docker:
	dep ensure
	mkdir -p build/docker
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/docker/app
	docker build -t $(IMAGE_NAME):$(VERSION) .
# Push docker image
publish:
	docker push $(IMAGE_NAME):$(VERSION)
