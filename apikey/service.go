package apikey

type Service interface {
	Get(key string) (ApiKey, error)
	List() ([]ApiKey, error)
	Delete(key string) (ApiKey, error)
	Valid(key string) (bool, error)
	Create(apiKey ApiKey) (ApiKey, error)
}

// NewApiKeyService returns a new reference to apikey.Service
func NewApiKeyService() Service {
}

