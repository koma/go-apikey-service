package apikey

type ApiKey struct {
    ID int64 `json:id`
    Key string `json:key`
    SourceType string `json:sourceType`
    Description string `json:description`
    DeployName string `json:deployName`
}