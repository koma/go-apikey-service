package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"gitlab.com/radionet/go-apikey-service/apikey"


)

func main() {
	// init router
	r := mux.NewRouter().StrictSlash(true)
	// base path
	api := r.PathPrefix("/apikey-service").Subrouter()
	// subrouters
	apiKeysRouter := api.PathPrefix("/apikeys").Subrouter()
	apiKeysController := apikey.NewController()
	apiKeysRouter.Path("/").
		Methods("GET").
		HandlerFunc(apiKeysController.GetList)
	apiKeysRouter.Use(jsonHandler, handlers.CompressHandler, stdoutLoggingHandler)

	server := &http.Server{
		Handler:      r,
		Addr:         ":8080",
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}
	log.Fatal(server.ListenAndServe())
}

// jsonHandler sets the Content-Type headers for JSON responses
func jsonHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json;charset=utf-8")
		next.ServeHTTP(w, r)
	})
}

// stdoutLoggingHandler logs the requests in apache logging format to standard out
func stdoutLoggingHandler(next http.Handler) http.Handler {
	return handlers.LoggingHandler(os.Stdout, next)
}
